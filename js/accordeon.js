function accordeon() {
  let accordeonTitle = document.querySelectorAll('.accordeon-title-js');

  accordeonTitle.forEach(el => {
    el.addEventListener('click', () => {
      if(el.parentElement.classList.contains('active')) {
        el.parentElement.classList.remove('active');
      } else {
        el.parentElement.classList.add('active');
      }
    });
  });
}

accordeon();